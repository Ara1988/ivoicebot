package com.ivoice.bot;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import com.ivoice.bot.actor.BotActor;

import java.util.Scanner;

public class IvoiceBotMain {
    public static void main(String[] args) {

        ActorSystem actorSystem = ActorSystem.create("Bot");
        Props props = Props.create(BotActor.class, BotActor::new);
        ActorRef botActor = actorSystem.actorOf(props, "bot_actor");

        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Bot: please, ask your question");
            System.out.print("User: ");
            while (true) {
                Thread.sleep(2000);
                if (botActor.isTerminated()) {
                    break;
                }

                String command = scanner.nextLine();
                botActor.tell(command.trim().toLowerCase(), botActor);
            }


        } catch (Exception e) {

        } finally {
            actorSystem.terminate();
        }
    }
}
