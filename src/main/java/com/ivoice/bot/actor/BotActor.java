package com.ivoice.bot.actor;

import akka.actor.AbstractActor;
import akka.actor.AbstractLoggingActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.japi.pf.ReceiveBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class BotActor extends AbstractLoggingActor {
    private final LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);


    @Override
    public Receive createReceive() {

        return receiveBuilder()
            .matchEquals(
                "y",
                s -> {
                    System.out.println("Bot: please, ask your question");
                    System.out.print("User: ");
                })
            .matchEquals(
                "",
                s -> {
                    System.out.println("Bot: please, ask your question");
                    System.out.print("User: ");
                })
            .matchEquals(
                "n",
                s -> {
                    getContext().system().terminate();
                })
            .matchAny(s -> {
                System.out.println(sendQuestion(s.toString()));
                System.out.println("Bot: Ask next question? Y/N");
                System.out.print("User: ");
            })
            .build();
    }


    private String sendQuestion(String s) {

        try (CloseableHttpClient client = HttpClients.createDefault();) {

            HttpPost httpPost = new HttpPost("https://odqa.demos.ivoice.online/model");

            String json = "{\"context\": [\"" + s + "\"]}";
            StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            CloseableHttpResponse execute = client.execute(httpPost);


            BufferedReader in = new BufferedReader(
                new InputStreamReader(execute.getEntity().getContent()));
            String inputLine;
            StringBuffer content = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                content.append(inputLine);
            }
            in.close();
            return content.toString().split("]")[0].substring(3);
        } catch (Exception e) {

        }

        return null;
    }
}
